<?php
return [
    'secret_key' => env('COINEX_SECRET_KEY', ''),
    'access_id' => env('COINEX_ACCESS_ID', '')
];
