<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

Route::get('/whale', [TestController::class,'whale_alert']);
Route::get('/test', [TestController::class,'test']);
Route::get('/', [TestController::class,'rsi_alert']);
Route::get('/info', [TestController::class,'orders_info']);
