<?php


namespace App\Classes;


use Http;

class Coinex
{
    protected $secret_key, $access_id;
    protected $base_url = 'https://api.coinex.com/perpetual/v1';

    public function __construct($access_id = null, $secret_key = null)
    {
        $this->secret_key = $secret_key != null
            ? $secret_key : config('coinex.secret_key');

        $this->access_id = $access_id != null
            ? $access_id : config('coinex.access_idn');
    }

    public function market_list()
    {
        $response = Http::get($this->url('/market/list'));
        if ($response->ok()) {
            return $response->json();
        }
        abort(500);
    }

    public function market_kline($market, $type = '1hour', $limit = 100)
    {
        $response = Http::get($this->url("/market/kline?market={$market}&type={$type}&limit={$limit}"));
        if ($response->ok()) {
            return $response->json();
        }
        abort(500);
    }

    private function url($service_path)
    {
        return $this->base_url . $service_path;
    }

}
