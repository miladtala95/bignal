<?php


namespace App\Classes;


use Http;

class WhaleAlert
{
    protected $secret_key;
    protected $base_url = 'https://api.whale-alert.io/v1';

    public function __construct($secret_key = null)
    {
        $this->secret_key = $secret_key != null
            ? $secret_key : config('whale_alert.secret_key');
    }

    public function transactions()
    {
        $response = Http::get($this->url('/transactions'));
        if ($response->ok()) {
            return $response->json();
        }
        abort(500);
    }

    private function url($service_path)
    {
        return $this->base_url . $service_path . '?api_key=' . $this->secret_key;
    }

}
