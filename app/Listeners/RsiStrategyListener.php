<?php

namespace App\Listeners;

use App\Events\DataReceivedEvent;
use App\Models\Meta;
use App\Models\Order;

class RsiStrategyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(DataReceivedEvent $event)
    {
        $new_data = $event->data;
        $pair = $new_data['pair'];

        $old_data = Meta::getData($new_data['pair'], null);

        if ($old_data) {
            $old_data = $old_data->data;
            if ($old_data['previous_candle']['rsi'] != $new_data['previous_candle']['rsi']
                || $old_data['previous_candle']['close'] != $new_data['previous_candle']['close']) {
                // new candle

                // implement strategy
                if ($new_data['previous_candle']['rsi'] < 30) {
                    print('buying '. $pair);
                    // buy
                    // price : $new_data['previous_candle']['close']
                    // amount wil be calculated
                    $price = doubleval($new_data['previous_candle']['close']);
                    $amount = 0;
                    if (Order::where([
                            ['pair', '=', $pair],
                            ['status', '=', 'pending']
                        ])->count() == 0)
                        Order::create([
                            'price' => $price,
                            'amount' => $amount,
                            'pair' => $pair,
                        ]);
                } elseif ($new_data['previous_candle']['rsi'] > 40) {
                    print('selling '. $pair);
                    // sell (close open position for $pair)
                    // price : $new_data['previous_candle']['close']
                    if (Order::where([
                            ['pair', '=', $pair],
                            ['status', '=', 'pending']
                        ])->count() == 1) {
                        $order = Order::where([
                            ['pair', '=', $pair],
                            ['status', '=', 'pending']
                        ])->first();
                        $order->close_price = $new_data['previous_candle']['close'];
                        $order->status = 'closed';
                        $order->save();
                    }
                }

            }
        }

        Meta::setData($new_data['pair'], $new_data);
    }
}
