<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $casts = [
        'key' => 'string',
        'data' => 'array'
    ];
    protected $fillable = ['key', 'data',];

    public static function setData($key, $data)
    {
        try {
            $meta = Meta::firstOrNew(['key' => $key,]);
            $meta->data = $data;
            $meta->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getData($key, $default)
    {
        return Meta::where('key', $key)->first() ?? $default;
    }
}
