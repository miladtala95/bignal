<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Morilog\Jalali\Jalalian;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'price',
        'close_price',
        'amount',
        'status',
        'pair',
        'leverage'
    ];

    public function getProfitAttribute()
    {
        if ($this->status == 'closed' && $this->close_price > 0 && $this->price > 0) {
            return (($this->close_price / $this->price) - 1) * 100;
        } else {
            return 0;
        }
    }

    public function getCreatedAtAttribute($value)
    {

        return Jalalian::fromCarbon(Carbon::parse($value))
            ->format('Y-m-d H:i:s');
    }

}
