<?php

namespace App\Http\Controllers;

use App\Classes\Coinex;
use App\Classes\WhaleAlert;
use App\Events\DataReceivedEvent;
use App\Http\Resources\OrderResource;
use App\Models\Meta;
use App\Models\Order;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use LupeCode\phpTraderNative\Trader;

class TestController extends Controller
{
    public function test(Request $request)
    {
        $new_data = [
            'pair' => 'BTCUSDT',
            'previous_candle' => [
                'rsi' => rand(1, 29),
                'close' => rand(100, 999),
                'ts' => 111
            ],
            'current_candle' => [
                'rsi' => rand(1, 29),
                'close' => rand(100, 999),
                'ts' => 222
            ],
        ];

        $pair = $new_data['pair'];

        $old_data = Meta::getData($new_data['pair'], null);

        if ($old_data) {
            $old_data = $old_data->data;
            if ($old_data['previous_candle']['rsi'] != $new_data['previous_candle']['rsi']
                || $old_data['previous_candle']['close'] != $new_data['previous_candle']['close']) {
                // new candle

                // implement strategy
                if ($new_data['previous_candle']['rsi'] < 30) {
                    // buy
                    // price : $new_data['previous_candle']['close']
                    // amount wil be calculated
                    $price = doubleval($new_data['previous_candle']['close']);
                    $amount = 100 / $price;
                    if (Order::where([
                            ['pair', '=', $pair],
                            ['status', '=', 'pending']
                        ])->count() == 0)
                        Order::create([
                            'price' => $price,
                            'amount' => $amount,
                            'pair' => $pair,
                        ]);
                } elseif ($new_data['previous_candle']['rsi'] > 40) {
                    // sell (close open position for $pair)
                    // price : $new_data['previous_candle']['close']
                    if (Order::where([
                            ['pair', '=', $pair],
                            ['status', '=', 'pending']
                        ])->count() == 1) {
                        $order = Order::where([
                            ['pair', '=', $pair],
                            ['status', '=', 'pending']
                        ])->first();
                        $order->close_price = $new_data['previous_candle']['close'];
                        $order->status = 'closed';
                        $order->save();
                    }
                }

            }
        }

        Meta::setData($new_data['pair'], $new_data);
    }

    public function whale_alert()
    {
        $whale = new WhaleAlert();
        $response = $whale->transactions();

        return $response;
    }

    public function get_rsi($pair)
    {
        $coinex = new Coinex();
        $response = $coinex->market_kline($pair, '5min');

        $closes = collect([]);
        foreach ($response['data'] as $item) {
            $closes->push(doubleval($item[2]));
        }
        $closes = $closes->reverse();
        $rsi = Trader::rsi($closes->toArray(), 10);

        return array_values($rsi)[0];
    }

    public function rsi_alert()
    {
        $cnx_url = 'https://api.coinex.com/v1';
        $coinex = new Coinex();
        $pairs = collect($coinex->market_list()['data'])->filter(function ($q) {
            return /*Str::startsWith($q, 'BTC') &&*/ /*Str::endsWith($q['name'], 'USDT')*/ true;
        });
        //$pairs = $pairs->take(2);
        //dd($pairs->pluck('name'));
        $client = new Client();
        $promises = [];
        foreach ($pairs as $item) {
            $pair = $item['name'];
            $promises[$pair] = $client->getAsync($cnx_url . '/market/kline?market=' . $pair . '&type=5min');
            //$promises['30_'.$pair] = $client->getAsync($cnx_url . '/market/kline?market=' . $pair . '&type=15min');
        }

        $responses = Utils::settle($promises)->wait();

        $page_response = [];
        foreach ($responses as $key => $response) {
            if ($response['state'] === 'rejected') continue;

            $result = $response['value'];
            //dd(json_decode($result->getBody() . '', true));
            $data = json_decode($result->getBody() . '', true)['data'];

            $closes = collect([]);
            foreach ($data as $item) {
                $closes->push(doubleval($item[2]));
            }
//            $closes = $closes->reverse();
            $rsi = Trader::rsi($closes->toArray(), 10);
            $macd = Trader::macd($closes->toArray(), 12, 24, 9);
            /*$event_data = [
                'pair' => $data[0][7],
                'previous_candle' => [
                    'rsi' => array_values($rsi)[count($rsi) - 2],
                    'close' => $closes->toArray()[count($closes->toArray()) - 2],
                    'time' => now()
                ],
                'current_candle' => [
                    'rsi' => array_values($rsi)[count($rsi) - 1],
                    'close' => $closes->toArray()[count($closes->toArray()) - 1]
                ],
            ];*/
            //event(new DataReceivedEvent($event_data));
//            dd($macd,trader_macd($closes->toArray(), 12, 24, 9));
            $macd_line = $macd['MACD'];
            $macd_signal = $macd['MACDSignal'];
            $macd_histogram = $macd['MACDHist'];
            $page_response[$key] = [
                'rsi' => array_values($rsi)[count($rsi) - 1],
                'macd_line' => array_values($macd_line)[count($macd_line) - 1],
                'macd_signal' => array_values($macd_signal)[count($macd_signal) - 1],
                'macd_histogram' => array_values($macd_histogram)[count($macd_histogram) - 1],
            ];
        }

        uasort($page_response, function ($a, $b) {
            return $a['rsi'] <=> $b['rsi'];
        });

        $response = Http::get('https://www.quandl.com/api/v3/datasets/BCIW/_INX.json?api_key=WqRvyP2U5meRKfHzFjBL');
        if ($response->successful())
            $sp = [
                'data' => $response->json()['dataset']['data'][0][4],
                'date' => $response->json()['dataset']['data'][0][0],
            ];
        else $sp = [
            'data' => '-',
            'date' => '-',
        ];
        return view('rsi_alert', compact('page_response', 'sp'));
        return [
            'data' => $page_response,
            'in' => (microtime(true) - $time_start)
        ];
    }

    public static function rsi_alert_console()
    {
        $cnx_url = 'https://api.coinex.com/v1';
        $pairs = [
            'BTCUSDT',
            'DOGEUSDT',
            'EOSUSDT',
            'ETHUSDT',
            'BCHUSDT',
            'LTCUSDT',
            'TRXUSDT',
            'ADAUSDT',
            'BNBUSDT',
            'XRPUSDT',
            'LINKUSDT',
            'UNIUSDT',
            'BSVUSDT',
            'DOTUSDT',
        ];
        $client = new Client();
        $promises = [];
        foreach ($pairs as $pair) {
            $promises[$pair] = $client
                ->getAsync($cnx_url . '/market/kline?market=' . $pair . '&type=5min');
        }

        $responses = Utils::settle($promises)->wait();

        foreach ($responses as $response) {
            if ($response['state'] === 'rejected') continue;

            $data = json_decode($response['value']->getBody() . '', true)['data'];

            $closes = collect([]);
            foreach ($data as $item) {
                $closes->push(doubleval($item[2]));
            }
            $rsi = Trader::rsi($closes->toArray(), 10);
            $event_data = [
                'pair' => $data[0][7],
                'previous_candle' => [
                    'rsi' => array_values($rsi)[count($rsi) - 2],
                    'close' => $closes->toArray()[count($closes->toArray()) - 2],
                    'ts' => $data[count($data) - 2][0]
                ],
                'current_candle' => [
                    'rsi' => array_values($rsi)[count($rsi) - 1],
                    'close' => $closes->toArray()[count($closes->toArray()) - 1],
                    'ts' => $data[count($data) - 1][0]
                ],
            ];
            event(new DataReceivedEvent($event_data));
        }
    }

    public function get_kline()
    {

    }

    function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return substr($haystack, 0, $length) === $needle;
    }

    function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if (!$length) {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }

    public function orders_info()
    {
        $orders = Order::all();

        $sum = 0.0;
        foreach ($orders->where('status', 'closed') as $order) {
            $sum += doubleval($order->profit);
        }

        $profits = [];
        foreach ($orders->where('status', 'closed') as $order) {
            if (isset($profits[$order->pair])) {
                $profits[$order->pair] = array_merge($profits[$order->pair], [$order->profit]);
            } else {
                $profits[$order->pair] = [$order->profit];
            }
        }

        return [
            'profit' => '% ' . number_format($sum, 2),
            'orders_count' => [
                'pending' => $orders->where('status', 'pending')->count(),
                'closed' => $orders->where('status', 'closed')->count(),
                'error' => $orders->where('status', 'error')->count(),
            ],
            'since' => Order::count() > 0 ? Order::orderBy('created_at', 'asc')->first()->created_at : '-',
            'orders' => [
                'open' => OrderResource::collection($orders->where('status', 'pending'))
            ],
            'profits' => $profits
        ];
    }

}
